package com.example.calculadorajava;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private EditText txtContraseña;
    private Button btnIng;
    private Button btnSalir;

    private Calculadora calculadora;

    private void iniciar(){
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
        btnIng = findViewById(R.id.btnIng);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void btnSalir(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("¿CERRAR APP?");
        confirmar.setMessage("Se descartará toda la información agregada");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                aceptar();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        confirmar.show();
    }

    private void aceptar(){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciar();
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSalir();
            }
        });
        btnIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = txtUsuario.getText().toString();
                String pass= txtContraseña.getText().toString();
                if(user.equals("FP03") && pass.equals("123")){
                    Intent intent = new Intent(MainActivity.this,Calculadora.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(),"USUARIO O CONTRASEÑA ERRONEA",Toast.LENGTH_SHORT);
                }
            }
        });
    }
}