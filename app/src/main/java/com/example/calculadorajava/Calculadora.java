package com.example.calculadorajava;

public class Calculadora {
    private float num1;
    private float num2;
    private String user;
    private String pass;

    public Calculadora(){
        this.num1 = 0.0f;
        this.num2 = 0.0f;
        this.user = "";
        this.pass = "";
    }
    public Calculadora(float num1, float num2, float res, String user, String pass){
        this.num1 = num1;
        this.num2 = num2;
        this.user = user;
        this.pass = pass;
    }
    public Calculadora(Calculadora calcula){
        this.num1 =calcula.num1;
        this.num2 =calcula.num2;
        this.user =calcula.user;
        this.pass =calcula.pass;
    }
    public float getNum1(){return num1;}
    public void setNum1(float num1){this.num1 = num1;}
    public float getNum2(){return num2;}
    public void setNum2(float num2){this.num2 = num2;}
    public String getUser(){return user;}
    public void setUser(String user){this.user = user;}
    public String getPass(){return pass;}
    public void setPass(String pass){this.pass = pass;}

    public float calcularTotalSum(){
        float totalS = 0.0f;
        totalS = this.num1 + this.num2;
        return totalS;
    }
    public float calcularTotalRes(){
        float totalR = 0.0f;
        totalR = this.num1 - this.num2;
        return totalR;
    }
    public float calcularTotalMul(){
        float totalM = 0.0f;
        totalM = this.num1 * this.num2;
        return totalM;
    }
    public float calcularTotalDiv(){
        float totalD = 0.0f;
        totalD = this.num1 / this.num2;
        return totalD;
    }
}
